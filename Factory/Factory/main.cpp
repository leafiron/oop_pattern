//
//  main.cpp
//  Factory
//
//  Created by PC on 1/17/22.
//
#include <string>
#include "AbstractFactory.hpp"
using namespace std;

// We may use a "map" technique...
unique_ptr<Product> make_product(const string type)
{
    if (!type.compare("group1"))
    {
        FactoryGroup1 f1;
        return f1.makeProduct();
    }
    else if (!type.compare("group2"))
    {
        FactoryGroup2 f2;
        return f2.makeProduct();
    }
    return nullptr;
};

int main(int argc, const char * argv[]) {
    // insert code here...
    
    unique_ptr<Product> g1 = make_product("group1");
    unique_ptr<Product> g2 = make_product("group2");
    
    g1->componentA->ProductSpecifications();
    g1->componentB->ProductSpecifications();
    
    g2->componentA->ProductSpecifications();
    g2->componentB->ProductSpecifications();

    getchar();
    return 0;
}
