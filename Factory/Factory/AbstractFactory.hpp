//
//  Factory.hpp
//  Factory
//
//  Created by PC on 1/17/22.
//

#ifndef Factory_hpp
#define Factory_hpp

#include <iostream>
#include <memory>
using namespace std;

struct ProductComponentAAbstract;
struct ProductComponentBAbstract;

// Define final product that contains component A and B
struct Product
{
    unique_ptr<ProductComponentAAbstract> componentA;
    unique_ptr<ProductComponentBAbstract> componentB;
    Product(unique_ptr<ProductComponentAAbstract> cA, unique_ptr<ProductComponentBAbstract> cB) {
        componentA = move(cA);
        componentB = move(cB);
    }
};


// Define component (e.g., IK???)
struct ProductComponentAAbstract
{
    ProductComponentAAbstract(){};
    virtual ~ProductComponentAAbstract() = default;
    virtual void ProductSpecifications() = 0;
};

struct ProducdtComponentAGroup1 : ProductComponentAAbstract
{
    virtual ~ProducdtComponentAGroup1() {};
    virtual void ProductSpecifications() override
    {
        cout << "I am component A in group 1" << endl;
    }
};

struct ProducdtComponentAGroup2 : ProductComponentAAbstract
{
    virtual ~ProducdtComponentAGroup2() {};
    virtual void ProductSpecifications() override
    {
        cout << "I am component A in group 2" << endl;
    }
};


struct ProductComponentBAbstract
{
    ProductComponentBAbstract() {};
    virtual ~ProductComponentBAbstract() = default;
    virtual void ProductSpecifications() = 0;
};

struct ProducdtComponentBGroup1 : ProductComponentBAbstract
{
    virtual ~ProducdtComponentBGroup1() {};
    virtual void ProductSpecifications() override
    {
        cout << "I am component B in group 1" << endl;
    }
};



struct ProducdtComponentBGroup2 : ProductComponentBAbstract
{
    virtual ~ProducdtComponentBGroup2() {};
    virtual void ProductSpecifications() override
    {
        cout << "I am component B in group 2" << endl;
    }
};



// Define factory for each group

struct FactoryAbstract
{
private:
    virtual unique_ptr<Product> makeProduct() = 0;
};


struct FactoryGroup1
{
    virtual unique_ptr<Product> makeProduct()
    {
        //ProducdtComponentAGroup1 pA = ProducdtComponentAGroup1();
        //ProducdtComponentBGroup1 pB = ProducdtComponentBGroup1();
        
        
        return make_unique<Product>(make_unique<ProducdtComponentAGroup1>(), make_unique<ProducdtComponentBGroup1>());
    }
};

struct FactoryGroup2
{
    virtual unique_ptr<Product> makeProduct()
    {
        //ProducdtComponentAGroup2 pA = ProducdtComponentAGroup2();
        //ProducdtComponentBGroup2 pB = ProducdtComponentBGroup2();
    
        return make_unique<Product>(make_unique<ProducdtComponentAGroup2>(), make_unique<ProducdtComponentBGroup2>());
    }
};

#endif /* Factory_hpp */
