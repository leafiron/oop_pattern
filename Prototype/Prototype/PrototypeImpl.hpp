//
//  PrototypeImpl.hpp
//  Prototype
//
//  Created by PC on 1/23/22.
//

#ifndef PrototypeImpl_hpp
#define PrototypeImpl_hpp

#include <stdio.h>
#include <string>
using namespace std;

class baseClass
{
public:
    baseClass() {};
    virtual baseClass* clone() = 0;
};

class baseClassConcrete : public baseClass
{
public:
    baseClassConcrete() : record1("Default string 1"), record2("Default string 2") {};
    virtual baseClass* clone() override
    {
        return new baseClassConcrete(*this);
    };
    
    
    void setRecord1(const string str1)
    {
        record1 = str1;
    };
    
    void setRecord2(const string str2)
    {
        record1 = str2;
    };
    
    string getRecord1()
    {
        return record1;
    };

    string getRecord2()
    {
        return record2;
    };
    
private:
    string record1;
    string record2;
};

#endif /* PrototypeImpl_hpp */
