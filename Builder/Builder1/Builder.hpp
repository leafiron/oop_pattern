//
//  Builder.hpp
//  Builder
//
//  Created by PC on 1/16/22.
//

#ifndef Builder_hpp
#define Builder_hpp

#include <stdio.h>

// Define "director"

class Product
{
public:
    Product(const double paramA = 0.0) : paramA(paramA) {
        // Set essential in the con'tor
    };
    virtual ~Product(){};
private:
    double paramA; // Essential param
    double paramB; // Optional param
    double paramC; // Optional param

public:
    // Check out no setter to prevent violations of OOP principle from too much expansion.
    void setterA(const double val) {paramA = val;};
    void setterB(const double val) {paramB = val;};
    void setterC(const double val) {paramC = val;};
    
    double getterA() {return paramA;};
    double getterB() {return paramB;};
    double getterC() {return paramC;};
};

class IBuilder {
protected:
    Product* pdt;
public:
    IBuilder() {};
    virtual ~IBuilder() {
        if (pdt)
            pdt = nullptr;
    };
    IBuilder* create(){
        pdt = new Product(1.5);
        return this;
    }
    
    Product* getProduct(){
        return pdt;
    }
public:
    // May or may not need for essential param setter
    // virtual void setParamA(const double val) = 0;
    virtual IBuilder* setParamB(const double val) = 0;
    virtual IBuilder* setParamC(const double val) = 0;
};

class Director
{
private:
    IBuilder* builderInstance;
    
public:
    Director(IBuilder* b) : builderInstance(b) {
        
    }
    ~Director(){};
    
public:
    Product* buildInstructionA(){
        return builderInstance->create()->setParamB(10)->setParamC(5)->getProduct();
    }
};

class ParamBBuilder : public IBuilder
{
public:
    // Default setter for each parameter?
    ParamBBuilder()
    {
    };
    
    ~ParamBBuilder() {};
    
    // builder interfaces
    virtual IBuilder* setParamB(const double val) override
    {
        pdt->setterB(val);
        return this;
    };
    
    virtual IBuilder* setParamC(const double val) override
    {
        pdt->setterC(val);
        return this;
    }
};

class ParamBCBuilder : public IBuilder
{
    // Default setter for each parameter?
    ParamBCBuilder()
    {
    };
    
    ~ParamBCBuilder()
    {
    };
    
    // builder interfaces
    virtual IBuilder* setParamB(const double val) override
    {
        pdt->setterB(val);
        return this;
    };
    
    virtual IBuilder* setParamC(const double val) override
    {
        pdt->setterC(val);
        return this;
    }
};

#endif /* Builder_hpp */
