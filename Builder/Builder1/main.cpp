//
//  main.cpp
//  Builder
//
//  Created by Seoungkyou Lee on 1/16/22.
//

// Builder pattern...
// 1. Use when a user want to segregate expression details from the product build process.
// 2. Composed of..
// a "Director", who are in charge of product building instruction, who refers to the specific builder under polymorphism.
// a "Builder", who provide interfaces for the product expression, who has a product as its member.
// a set of derivatives who inherits the builder and defines specific expressions of a product.

/* https://4z7l.github.io/2021/01/19/design_pattern_builder.html#:~:text=%EB%B9%8C%EB%8D%94%20%ED%8C%A8%ED%84%B4%EC%9D%80%20%EB%B3%B5%EC%9E%A1%ED%95%9C%20%EA%B0%9D%EC%B2%B4,%ED%95%98%EB%8A%94%20%EB%B0%A9%EB%B2%95%EC%9D%84%20%EC%A0%9C%EA%B3%B5%ED%95%9C%EB%8B%A4.
 */

#include <stdio.h>
#include <iostream>
#include "Builder.hpp"
using namespace std;

int main(){
    Director d(new ParamBBuilder());
    
    Product *p = d.buildInstructionA();

    cout << p->getterA() << "  " << p->getterB() << "  " << p->getterC() << endl;
    return 0;
}
